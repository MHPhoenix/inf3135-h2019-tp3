
default: outils.o tp3.o outils.h structure.h
	gcc -Wall -pedantic -std=c99 -lm -O2 -o tp3 tp3.c outils.c structure.c 

tp3.o: tp3.c
	gcc -Wall -pedantic -std=c99   -c tp3.c

outils.o: outils.c
	gcc -Wall -pedantic -std=c99 -lm   -c outils.c

test:
	for testeur in `ls data/*.*` ; do \
	./tp3 -c $(CP) -i $$testeur >> resultat.txt; \
	done

clean:
	rm -rf  *.o
	rm -rf tp3
	rm -rf code.txt

data:
	wget -N https://www.github.com/guyfrancoeur/INF3135_H2019/raw/master/tp1/data.zip
	unzip -o data -d ./data

.phony: data clean

resultat:
	git add resultat.txt
	git commit -m "MAKE RESULTAT"
	git push origin master

r:
	make
	./tp3 -c MONH99519906 -i nom_du_fichier_en_entree.ext 


CP = `cat cp.txt`

valgrind:
	valgrind -v ./tp3 -c $(CP) -i testeur.txt
