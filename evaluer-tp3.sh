#!/bin/bash
# evaluer.sh

indice=0
point=0
temps=0
let codeRetourTp;
codeRetour=0
commande=0
succes='reussi'
echec='echec'
noteFinal="Note (total) pour $(cat cp.txt) dans inf3135-h2019-tp3: "
total=0


if [ -z $1 ]

then

  correction=inf3135-h2019-tp3.correction

else

  correction=$1

fi

wget -q https://github.com/guyfrancoeur/INF3135_H2019_TP3/raw/master/${correction} -O ${correction}

if [ ! -f inf3135-h2019-tp3.correction ]

then

  echo "erreur fatale, fichier correction inexistent."
  exit 1

fi

for line in `cat ${correction}`

do

  # fichier (nom de la variable), 0 (position), 1 (nombre de caracteres)
                        point=${fichier:0:1}

                        temps=${fichier:2:2}

                            if [ "$temps" == 00]; then
                                temps=20
                            fi

                        codeRetour=${fichier:5:1}

                        commande=${fichier:27}
                        eval 'timeout' $temps's' $commande &>/dev/null  #(pour automatiser les tests)

                        codeRetourTp=$?

                        if [ "$codeRetour" = "$codeRetourTp" ]; then
                                echo "$indice : $succes $point pts"
                                total=$((total + point))

                        else
                            	echo "$indice : $echec"
                        fi

                        indice=$((indice + 1))


done < inf3135-h2019-tp3.correction

echo "$noteFinal $total pts"
echo "FIN."
