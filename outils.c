//code source des fonctions

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include "structure.h"
#include "outils.h"


uint128_t racineCarre (uint128_t nombre) {

	uint128_t racine = 1;
	uint128_t n = 1;
	uint128_t k = 0;

	do {

		++racine;
		k = n;
		n = racine*racine;

	} while (n <= nombre && k < nombre);

	--racine;

	return racine;

}


uint128_t puissance (uint128_t nombre, uint128_t laPuissance) {

	uint128_t p = 1;

	for (uint128_t i = 1; i <= laPuissance; i++) {

		p = p * nombre;
	}

	return p;
}



bool calculNombrePremier (uint128_t nombre) {

        uint128_t racine = racineCarre ( nombre );

        if (nombre  == 2) {
                return true;
        }

	if ( (nombre % 2) == 0) {
                return false;
        } else {

                int i;

                for (i = 3; i <= racine; i = i + 2) {

                        if ( (nombre % i) == 0) {
                                return false;
                        }
                }

                return true;
        }
}


void calculNombreParfait (Argument argument, Vector *vecteur, uint128_t  debut, uint128_t fin) {


        uint128_t nombrePremier=2;
        uint128_t nombreParfait;

        while ( (nombrePremier < fin) && (nombrePremier < 32) ) {

                if ( calculNombrePremier(nombrePremier) ) {

                        if ( calculNombrePremier(puissance(2,nombrePremier) - 1) ) {

                                nombreParfait = ( (puissance(2, nombrePremier - 1)) * (puissance(2,nombrePremier) - 1) );

                                if (nombreParfait >= debut && nombreParfait <= fin ) {

					if (!recherche(vecteur,nombreParfait))  {
						appendV (vecteur, nombreParfait);

					}
                                }
                        }

                }

        	nombrePremier++;
	}
}


bool estValeurNumerique (char valeur []) {

	bool estNumerique = true;
	int i;

	for (i = 0; i < strlen(valeur); i++) {

		if ( !(valeur[i] >= '0' && valeur[i] <= '9') ) {
			estNumerique = false;
		}

	}

	return estNumerique;
}


uint128_t atoi_m (char valeur []) {

	uint128_t valeurNumerique = 0;
	int i;

	for (i = 0; i < strlen(valeur); i++) {

		valeurNumerique = (valeurNumerique * 10) +  (  valeur[i] - '0' );
	}

	return valeurNumerique;
}


int lecteurInterval (Argument argument, uint128_t *valeurDebut, uint128_t *valeurFin) {

        uint128_t temporaire = 0;
	char valeur1[100];
	char valeur2[100];
	int valeur = fscanf (argument.fichierDEntree, "%s %s", valeur1, valeur2);
	int valide = 0;

	if (valeur == 2 ) {

		if   (  estValeurNumerique(valeur1) &&  estValeurNumerique(valeur2) ) {

			valide = 1;
			*valeurDebut = atoi_m(valeur1);
			*valeurFin = atoi_m(valeur2);

			if (*valeurDebut > *valeurFin) {
				temporaire = *valeurDebut;
				*valeurDebut = *valeurFin;
				*valeurFin = temporaire;
                       	 }
		}
	}

	return valide;
}


void fermetureDesFichiers (Argument argument) {

        //char codePermanent [13] = "MONH08519906";

        if (argument.argument_i == 1) {
                fclose (argument.fichierDEntree);
        }

	if (argument.argument_o == 1) {
                fclose (argument.fichierDeSorti);
        }

	if (argument.argument_c == 1) {

                FILE *fichierASortir = fopen ("code.txt", "w");
                fputs (argument.codePermanent, fichierASortir);
		fclose (fichierASortir);
        }

}

Argument gestionArguments(int argc, char *argv[]) {

        Argument argument;
        argument.argument_c = 0;
        argument.argument_i = 0;
        argument.argument_o = 0;
        argument.argument_d = 0;
        argument.ordreAffichage = DECROISSANT;
        argument.fichierDEntree = stdin;
        argument.fichierDeSorti = stdout;
        char nomFichierEntre [200];
        char nomFichierSortie [200];
        int i;

        for (i = 1; i < argc; i = i+2) {

                if ( strcmp (argv [i], "-c") == 0) {

                        //code permanent
                        argument.argument_c = 1;

                        if (i < ( argc-1)) {

				if (strlen (argv [i + 1]) == 12) {
                                strcpy (argument.codePermanent, argv [i + 1]);

                                } else {
                                        exit (2);
                                }
                        }

                        else  {
                               	fprintf(stderr, "Usage: %s <-c CODEpermanent> [-i fichier.in] [-o fichier.out] \n", argv[0]);
                                exit(1);
                        }

                } else if ( strcmp (argv [i], "-i") == 0) {

                        //fichier d'entree
                        argument.argument_i = 1;

                        if (strlen (argv [i + 1] ) != 0) {
                                strcpy (nomFichierEntre, argv [i + 1]);
                                argument.fichierDEntree = fopen (nomFichierEntre, "r");
                        }

                } else if ( strcmp (argv [i], "-o") == 0 ) {

                        // fichier de sortie
			argument.argument_o = 1;

                        if (strlen (argv [i + 1]) != 0) {
                                strcpy (nomFichierSortie, argv [i + 1]);
                                argument.fichierDeSorti = fopen (nomFichierSortie, "w");

                                if (argument.fichierDeSorti == NULL) {
                                        exit (6);
                                }
                        }

                } else if ( strcmp (argv [i], "-d") == 0 )   {

			// pour l'ordre de sortie (croissant et decroissant)
			argument.argument_d = 1;

				if ( strlen (argv [i + 1]) != 0) {

                                	 if ( strcmp ("ASC", argv [i + 1]) == 0 ) {

						argument.ordreAffichage = CROISSANT;

					} else if (strcmp ("DES", argv [i + 1]) == 0) {

						argument.ordreAffichage = DECROISSANT;

					} else  {
						exit(3);
					}

				} else  {
					 exit(7);
                                }


		} else if ( (strcmp (argv [i], "-c") != 0) || ( strcmp (argv [i], "-i") != 0)
                                || ( strcmp (argv [i], "-o") != 0) || ( strcmp (argv [i], "-d") != 0)  ) {

			exit(3);
               }


        }


	if ( ( (argument.argument_i == 0) && (argument.argument_o == 0) && (argument.argument_c == 0) ) ||
             (  ((argument.argument_i == 1) || (argument.argument_o == 1))  && (argument.argument_c == 0)  )    ) {

                fprintf(stderr, "Usage: %s <-c CODEpermanent> [-i fichier.in] [-o fichier.out] \n", argv[0]);

                exit(1);
        }

	return argument;
}
