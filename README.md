# Travail pratique 3

## Description

Le cours de construction et maintenance de logiciel (INF3135), qui se donne
a l'Universite du Quebec a Montreal, a pour but de nous initier en
construction et maintenance de logiciel et au langage C.

Le troixieme travail pratique, est une maintenance du travail pratique 1 ou on automatise les tests avec
des fichiers bash, on utilise valgrin pour la memoire, les structures et enumerations ainsi que les TDA.

## Auteur

Houefa Orphyse Peggy Merveille MONGBO (MONH08519906)

## Fonctionnement

Le programme peut être lancé en ligne de commande avec au minimum les deux syntaxes suivantes :

> $ ./tp3 -d DES -c CODE_permanent < fichier.in > fichier.out

> $ ./tp3 -c CODE_permanent -d DES -i fichier.in > fichier.out

> Ou DES et ASC, sont l'ordre d'affichage decroissant ou croissant des nombres parfaits trouves.


## Contenu du projet

> **Le projet contient les fichiers suivants :**

> - cp.txt: contient mon code permanent complet en majuscule.
> - tp3.c : contient la fonction main.
> - README.md : contient la description du projet.
> - Makefile : supporte les appels make, make clean, make data, make test et make resultat.
> - .gitignore : contient les fichiers . et ce qu'on ce ne veut pas faire apparaitre dans git status.
> - outils.c : contient le code source du projet (inf3135-h2019-tp2)
> - outil.h : contient les prototypes des fonctions de outils.c
> - evaluer.sh : le bash du tp2 qui automatise les tests et retourne la note totale
> - evaluer-tp3.sh : le bash qui automatise les tests et retourne la note totale
> - structure.h : contient les prototypes de la fonction structure.c
> - structure.c : contient les differences fonctions de structure du tp


## References

> **Documentation :**

> - notes de cours
> - OpenClassRoom

## Statut

Fonctionnel

