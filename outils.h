#ifndef OUTILS_H

#define OUTILS_H

typedef enum ordre {

		CROISSANT = 0,
		DECROISSANT = 1
	} Ordre;


typedef struct Argument Argument;


struct Argument {

	int argument_c;

	char codePermanent [13];

	int argument_i;

	FILE * fichierDEntree;

	int argument_o;

	FILE * fichierDeSorti;

	int argument_d;

	Ordre ordreAffichage;

	} ;

uint128_t puissance (uint128_t nombre, uint128_t laPuissance);
uint128_t racineCarre (uint128_t nombre);
bool calculNombrePremier (uint128_t nombre);
bool estValeurNumerique (char valeur []);
uint128_t atoi_m (char valeur []);
int lecteurInterval (Argument argument, uint128_t *valeurDebut, uint128_t *valeurFin);
void fermetureDesFichiers (Argument argument);
Argument gestionArguments(int argc, char *argv[]);
void calculNombreParfait (Argument argument, Vector * vecteur , uint128_t debut, uint128_t fin);

#endif // pas necesssaire dde mettre le endif quand on a juste une seule instruction if
