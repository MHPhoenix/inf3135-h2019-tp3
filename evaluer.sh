#!/bin/bash
# evaluer.sh

wget -q https://github.com/guyfrancoeur/INF3135_H2019_TP2/raw/master/inf3135-h2019-tp2.correction

indice=0
point=0
temps=0
let codeRetourTp;
codeRetour=0
commande=0
succes='reussi'
echec='echec'
noteFinal="Note (total) pour $(cat cp.txt) dans inf3135-h2019-tp2: "
total=0

if [ ! -f inf3135-h2019-tp2.correction ]
then 
	exit 1
else
 
	while read fichier;

		do
			# fichier (nom de la variable), 0 (position), 1 (nombre de caracteres)
			point=${fichier:0:1}			

			temps=${fichier:2:2}

			codeRetour=${fichier:5:1}

			commande=${fichier:29}
			eval 'timeout' $temps's' $commande &>/dev/null  #(pour automatiser les tests)
			
			codeRetourTp=$?

			if [ "$codeRetour" = "$codeRetourTp" ]; then
				echo "$indice : $succes $point pts"
				total=$((total + point))
			
			else
				echo "$indice : $echec"
			fi 
			
			indice=$((indice + 1))

done < inf3135-h2019-tp2.correction

echo "$noteFinal $total pts"
echo "FIN."

fi
