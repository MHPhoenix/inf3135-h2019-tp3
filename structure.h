//prototypes du code source de la structure des données utilisées

#ifndef STRUCTURE_H

#define STRUCTURE_H

#include <stdbool.h>

__extension__ typedef unsigned __int128 uint128_t;


typedef struct {
  int size;	 // slots used so far
  int capacity;  // total available slots
  uint128_t *data;     // array of integers we're storing
} Vector;


void initV(Vector *vector, int capacity);
void appendV(Vector *vector, uint128_t value);
int getV(Vector *vector, int index);
void setV(Vector *vector, int index, int value);
void extendV(Vector *vector);
void freeV(Vector *vector);
void afficherCroissant (FILE * fichierDeSorti, Vector *vector);
void afficherDecroissant (FILE * fichierDeSorti, Vector *vector);
void tricroissant(uint128_t tab[], int tab_size);
bool recherche (Vector *vecteur, uint128_t nombreParfait);

#endif // pas necesssaire dde mettre le endif quand on a juste une seule instruction if


