//code source du projet et fonction main

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include "structure.h"
#include "outils.h"

int main (int argc, char *argv[]) {

	Argument argument;
        argument = gestionArguments(argc, argv);
	uint128_t valeurDebut = 0;
	uint128_t valeurFin = 0;
	int bonInterval = 0;

	Vector vecteur;

        initV(&vecteur,2);

	// Parcours tous les intervals
	if (argument.fichierDEntree != NULL) {

        	while ( !feof(argument.fichierDEntree) ) {

			if (lecteurInterval (argument, &valeurDebut, &valeurFin) == 1) {
				bonInterval++;
				calculNombreParfait(argument, &vecteur,  valeurDebut, valeurFin);
			}
                }

        } else {
                exit(5);
        }


	if (bonInterval == 0) {
		exit(4);
	}

	tricroissant(vecteur.data, vecteur.size );


	if (argument.argument_d == 0) {

		//affichage descendant
		afficherDecroissant(argument.fichierDeSorti, &vecteur);

	} else {

		if (argument.ordreAffichage == CROISSANT) {

			//affichage ascendant
			afficherCroissant(argument.fichierDeSorti, &vecteur);

		} else {

			//affichage descendant
			afficherDecroissant(argument.fichierDeSorti, &vecteur);
		}

	}

	fermetureDesFichiers (argument);

        return 0;
}


