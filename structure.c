//contient le code source de la structure des données utilisées
#include <stdio.h>
#include <stdlib.h>
#include "structure.h"


void initV(Vector *vector, int capacity) {

  // initialize size and capacity
  vector->size = 0;
  vector->capacity = capacity;

  // allocate memory for vector->data
  vector->data = malloc(sizeof(uint128_t) * vector->capacity);

  //init elements with zero
  //setV(vector, -1, 0);  //et verifier avec valgrind
  for (int i=vector->size;i < vector->capacity; ++i) {
    setV(vector, i, 0);
  }

}


void appendV(Vector *vector, uint128_t value) {
  extendV(vector);

  // append the value and increment vector->size
  vector->data[vector->size++] = value;
}


int getV(Vector *vector, int index) {
  if (index >= vector->size || index < 0) {
    printf("Index %d out of bounds for vector of size %d\n", index, vector->size);
    exit(1);
  }
  return vector->data[index];
}


void setV(Vector *vector, int index, int value) {
  if (index < vector->size && index >= 0) {
    vector->data[index] = value;
  }
}


void extendV(Vector *vector) {
  if (vector->size >= vector->capacity) {
    // double vector->capacity and resize the allocated memory accordingly
    vector->capacity *= 2;
    vector->data = realloc(vector->data, sizeof(uint128_t) * vector->capacity);
  }
}


void freeV(Vector *vector) {
  free(vector->data);
}


void afficherCroissant(FILE * fichierDeSorti, Vector *vector) {

  //fprintf(fichierDeSorti, "\n");
  for (int i=0;i < vector->size; ++i) {
    	fprintf(fichierDeSorti, "%llu "  ,vector->data[i]);
	fprintf(fichierDeSorti, "\n");
  }
    //fprintf(fichierDeSorti, "\n");
}


void afficherDecroissant(FILE * fichierDeSorti, Vector *vector) {
 // fprintf(fichierDeSorti, "\n(");
  for (int i = vector->size-1 ; i >= 0; --i) {
        fprintf(fichierDeSorti, "%llu "  ,vector->data[i]);
	fprintf(fichierDeSorti, "\n");
  }

}


void tricroissant(uint128_t * tab, int tab_size) {

	int i=0;
	uint128_t tmp=0;
	int j=0;

	for(i = 0; i < tab_size; i++) {

		for(j = 1; j < tab_size; j++) {

			if(tab[i] < tab[j]) {

				tmp = tab[i];
				tab[i] = tab[j];
				tab[j] = tmp;
				j--;
			}
		}
	}

	tmp = tab[0];

	for(i = 0; i < tab_size; i++) {

		tab[i] = tab[i+1];
		tab[tab_size-1] = tmp;
	}

}

bool recherche (Vector *vecteur, uint128_t nombreParfait) {

	bool trouve = false;
	int i = 0;

	while (!trouve && (i < vecteur->size) ) {

		if (vecteur -> data[i] == nombreParfait) {
			trouve = true;
		}
	i++;

	}

	return trouve;
}
